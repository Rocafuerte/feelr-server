setup:
	image deps.get compile ecto.setup

image:
	docker build -t feelr_app .

deps.get:
	docker-compose run --rm app mix deps.get

compile:
	docker-compose run --rm app mix compile

ecto.setup:
	docker-compose run --rm app mix ecto.setup

ecto.drop:
	docker-compose run --rm app mix ecto.drop

db-shell:
	docker-compose exec db psql -U postgres

iex:
	docker-compose exec app iex

server:
	docker-compose up -d
