use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or you later on).
config :Feelr, Feelr.Endpoint,
  secret_key_base: "RiaSgeXfP9r9gfiEbJG0lD81+VbgAvcvj7ItCBnhfCKzAYcSzTbMZmGKp1crDuZt"

# Configure your database
config :Feelr, Feelr.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "Feelr_prod",
  pool_size: 20
