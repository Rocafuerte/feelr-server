defmodule Feelr.Repo.Migrations.CreateSong do
  use Ecto.Migration

  def change do
    create table(:songs) do
      add :title, :string
      add :url, :string

      add :artist_id, references(:artists, on_delete: :nothing)

      timestamps()
    end

    create index(:songs, [:artist_id])
  end
end
