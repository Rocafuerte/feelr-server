defmodule Feelr.Repo.Migrations.CreateImage do
  use Ecto.Migration
  import Timex.Ecto.Timestamps

  def change do
    create table(:images) do
      add :original_url, :string
      add :standard_url, :string
      add :thumbnail_url, :string

      timestamps()
    end
  end
end
