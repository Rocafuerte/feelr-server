defmodule Feelr.Repo.Migrations.CreateTag do
  use Ecto.Migration

  def change do
    create table(:tags) do
      add :song_id, references(:songs, on_delete: :nothing)
      # add :option_id, references(:tag_category_options, on_delete: :nothing)
      timestamps()
    end

    # create index(:tag_category_options, [:option_d])
  end
end
