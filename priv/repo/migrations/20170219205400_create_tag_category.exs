defmodule Feelr.Repo.Migrations.CreateTagCategory do
  use Ecto.Migration

  def change do
    create table(:tag_categories) do
      add :name, :string
      add :is_default, :bool

      timestamps()
    end

  end
end
