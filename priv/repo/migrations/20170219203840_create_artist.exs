defmodule Feelr.Repo.Migrations.CreateArtist do
  use Ecto.Migration

  def change do
    create table(:artists) do
      add :name, :string
      add :bio, :string
      add :location, :string
      add :url, :string

      timestamps()
    end

  end
end
