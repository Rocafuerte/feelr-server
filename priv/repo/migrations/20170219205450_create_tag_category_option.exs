defmodule Feelr.Repo.Migrations.CreateTagCategoryOption do
  use Ecto.Migration

  def change do
    create table(:tag_category_options) do
      add :name, :string
      # add :tag_id, references(:tags, on_delete: :nothing)
      add :tag_category_id, references(:tag_categories, on_delete: :nothing)

      timestamps()
    end

  end
end
