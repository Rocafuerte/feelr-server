defmodule Feelr.Repo.Migrations.CreateFavourite do
  use Ecto.Migration

  def change do
    create table(:favourites) do
      add :song_id, references(:songs, on_delete: :nothing)
      timestamps()
    end

  end
end
