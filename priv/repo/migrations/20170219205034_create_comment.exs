defmodule Feelr.Repo.Migrations.CreateComment do
  use Ecto.Migration

  def change do
    create table(:comments) do
      add :body, :string
      add :user_id, references(:users, on_delete: :nothing)
      add :song_id, references(:songs, on_delete: :nothing)

      timestamps()
    end

    create index(:comments, [:user_id])
  end
end
