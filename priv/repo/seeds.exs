# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Feelr.Repo.insert!(%Feelr.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Feelr.Repo
alias Feelr.User
alias Feelr.Song
alias Feelr.Image
alias Feelr.Artist
alias Feelr.Comment
alias Feelr.Favourite
alias Feelr.Tag
alias Feelr.TagCategory
alias Feelr.TagCategoryOption

Repo.insert!(%User{username: "Ryan Swapp", email: "ryan@ryan.com", password: "passwrd", password_hash: "hashword"})
Repo.insert!(%User{username: "Rosie", email: "rosie@mydog.com", password: "11passwrd", password_hash: "rehashword"})

Repo.insert!(%Artist{
      name: "Test artist",
      bio: "short bio",
      location: "swede",
      url: "urllxz"
    })
Repo.insert!(%Artist{
      name: "Test artist",
      bio: "short bio",
      location: "swede",
      url: "urllxz"})

for _ <- 1..10 do
  Repo.insert!(%Song{
    title: Faker.Lorem.sentence,
    url: Faker.Lorem.sentence,
    artist_id: [1, 2] |> Enum.take_random(1) |> hd
  })
end

for _ <- 1..10 do
  Repo.insert!(%Image{
    original_url: Faker.Internet.image_url,
    standard_url: Faker.Internet.image_url,
    thumbnail_url: Faker.Internet.image_url
  })
end

for _ <- 1..10 do
  Repo.insert!(%Comment{
    body: Faker.Lorem.sentence,
    user_id: [1, 2] |> Enum.take_random(1) |> hd
  })
end

Repo.insert!(%TagCategory{
      name: "Mood",
      is_default: true
})

moods = ["Happy", "Sad", "Mad", "Glad", "Vad"]

o = nil

for mood <- moods do
    o = Repo.insert!(%TagCategoryOption{
          name: mood,
          tag_category_id: 1})
end

for _ <- 1..10 do
    Repo.insert!(%Tag{
      song_id: 1..10 |> Enum.take_random(1) |> hd,
      option: o
    })
end

