# feelr-server

## Getting up and running
```
$ make setup
$ make server
# Starts server at http://localhost:4000
```

## Graphiql

To run test queries against the graphql api, go to [`localhost:4000/graphiql`](http://localhost:4000/graphiql)
and use the web interface.

## Main dependencies

  * Phoenix - MVC-like framework http://www.phoenixframework.org/
  * Absinthe - Graphql library http://absinthe-graphql.org/
  * Guardian - Authentication library https://github.com/ueberauth/guardian

## Learning

Great tutorial series on using Graphql together with Phoenix:

  * https://ryanswapp.com/2016/11/29/phoenix-graphql-tutorial-with-absinthe/
  * https://ryanswapp.com/2016/12/03/phoenix-graphql-tutorial-with-phoenix-add-crud-using-mutations/
  * https://ryanswapp.com/2016/12/06/phoenix-graphql-tutorial-with-absinthe-authentication-with-guardian/