defmodule Feelr.FavouriteTest do
  use Feelr.ModelCase

  alias Feelr.Favourite

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Favourite.changeset(%Favourite{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Favourite.changeset(%Favourite{}, @invalid_attrs)
    refute changeset.valid?
  end
end
