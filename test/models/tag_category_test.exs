defmodule Feelr.TagCategoryTest do
  use Feelr.ModelCase

  alias Feelr.TagCategory

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = TagCategory.changeset(%TagCategory{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = TagCategory.changeset(%TagCategory{}, @invalid_attrs)
    refute changeset.valid?
  end
end
