defmodule Feelr.Schema.Types do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: Feelr.Repo

  scalar :time, description: "ISOz time" do
    parse &Timex.parse!(&1.value, "{ISO:Extended:Z}")
    serialize &Timex.format!(&1, "{ISO:Extended:Z}")
  end

  object :user do
    field :id, :id
    field :name, :string
    field :email, :string
    field :posts, list_of(:post), resolve: assoc(:posts)
  end

  object :image do
    field :thumbnail, :string
  end

  object :coordinates do
    field :lon, :float
    field :lat, :float
  end

  object :post do
    field :id, :id
    field :title, :string
    field :description, :string
    field :price, :float
    field :order_deadline, :time
    field :available_from, :time
    field :available_until, :time
    field :image, :image
    field :coordinates, :coordinates
    field :quantity, :integer
    field :user, :user, resolve: assoc(:user)
  end

  object :session do
    field :token, :string
  end
end
