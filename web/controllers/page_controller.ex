defmodule Feelr.PageController do
  use Feelr.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
