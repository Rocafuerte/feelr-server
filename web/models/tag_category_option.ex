defmodule Feelr.TagCategoryOption do
  use Feelr.Web, :model

  schema "tag_category_options" do
    field :name, :string

    belongs_to :tag_category, Feelr.TagCategory

    timestamps
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name])
    |> validate_required([:tag_id, :tag_category_id])
  end
end
