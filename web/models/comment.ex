defmodule Feelr.Comment do
  use Feelr.Web, :model

  schema "comments" do
    field :body, :string

    belongs_to :user, Feelr.User
    belongs_to :song, Feelr.Song

    timestamps
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:body])
    |> validate_required([:user_id, :song_id])
  end
end
