defmodule Feelr.Tag do
  use Feelr.Web, :model

  schema "tags" do
    has_one :option, Feelr.TagCategoryOption
    belongs_to :song, Feelr.Song

    timestamps
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [])
    |> validate_required([:song_id])
  end
end

# schema "tags" do
#   field :slug, :string, primary_key: True
#   field :label, :string

#   many_to_many :posts, FeelrPost, join_through: Feelr.PostTag

#   timestamps
# end
