defmodule Feelr.Song do
  use Feelr.Web, :model

  schema "songs" do
    field :title, :string
    field :url, :string

    has_one :image, Feelr.Image

    # REMEMBER LAST NIGHT? YOU DONT WANT TO DO IT
    # hehe jk, check out how tag category options look like
    has_many :comments, Feelr.Comment
    has_many :tags, Feelr.Tag
    has_many :favourites, Feelr.Favourite

    belongs_to :artist, Feelr.Artist

    timestamps
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :url, :artist_id])
    |> validate_required([:title, :url, :artist_id])
  end
end
