defmodule Feelr.TagCategory do
  use Feelr.Web, :model

  schema "tag_categories" do
    field :name, :string
    field :is_default, :boolean

    has_many :options, Feelr.TagCategoryOption

    timestamps
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [])
    |> validate_required([])
  end
end
