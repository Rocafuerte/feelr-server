defmodule Feelr.Artist do
  use Feelr.Web, :model

  schema "artists" do
    field :name, :string
    field :bio, :string
    field :location, :string
    field :url, :string

    has_one :image, Feelr.Image

    timestamps
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name,
                    :bio,
                    :location,
                    :url])
    |> validate_required([])
  end
end
