defmodule Feelr.Favourite do
  use Feelr.Web, :model

  schema "favourites" do
    belongs_to :song, Feelr.Song

    timestamps
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [])
    |> validate_required([:song_id])
  end
end
