defmodule Feelr.Image do
  use Feelr.Web, :model

  schema "images" do
    field :original_url, :string
    field :standard_url, :string
    field :thumbnail_url, :string

    timestamps
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [])
    |> validate_required([])
  end
end
